<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'Api'
], function ($router) {
    Route::post('login', 'AuthController@login')->name('api.login');
    Route::post('register', 'AuthController@register')->name('api.register');
    Route::post('logout', 'AuthController@logout')->name('api.logout');
    Route::post('refresh', 'AuthController@refresh')->name('api.refresh');
    Route::post('me', 'AuthController@me')->name('api.me');
});

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api'
], function (){
    Route::post('books', 'BookController@store')->name('books.store');
    Route::get('books', 'BookController@index')->name('books.filter');
    Route::get('reviews', 'ReviewController@index');
    Route::post('books/{id}/reviews', 'ReviewController@storeReview')->name('book.store.review');
});
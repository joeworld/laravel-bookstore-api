<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'review', 'comment', 'user_id'
    ];

	/**	
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\ReviewFactory;
use App\Http\Requests\Api\BookReviewRequest;

class ReviewController extends Controller
{

    private $review;

    public function  __construct(ReviewFactory $review)
    {
        $this->middleware('auth:api', ['except' => ['show']]);
        $this->review = $review::createApi();
    }

    public function storeReview($id, BookReviewRequest $request)
    {
        return $this->review->create($id, $request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
}

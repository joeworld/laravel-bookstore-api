<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\BookFactory;
use App\Http\Requests\Api\CreateBookRequest;
use App\Http\Resources\BookResource;
use App\Book;
use App\Http\Requests\Api\FilterRequest;

class BookController extends Controller
{

    private $book;

    public function  __construct(BookFactory $book)
    {
        $this->middleware('auth:api', ['except' => ['index']]);
        $this->middleware('is_admin', ['except' => ['index']]);
        $this->book = $book::createApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FilterRequest $request)
    {
       return $this->book->handleFilter($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBookRequest $request)
    {
        return $this->book->create($request);
    }
    
}
<?php

namespace App\Repositories\Api;
use App\Http\Requests\Api\CreateBookRequest;

use App\User;
use App\Book;
use App\Author;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Contracts\RepositoryInterface;
use App\Repositories\Api\UserApiRepository as UserRepo;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Http\Requests\Api\FilterRequest;

class BookApiRepository implements RepositoryInterface
{

	private $user_repo;

	public function __construct()
	{
		$this->user_repo = new UserRepo;  
	}

    public function get($type, $value)
	{
		return Book::where($type, $value)->first();
	}
    
	public function getAll($order = null, $limit = null)
	{}

	public function updateAvgReview($book_id)
	{
		$book = Book::find($book_id);
		$avg = round(DB::table('reviews')->where('book_id','=',$book_id)->avg('review'));
		$book->update([
			'avg_review' => $avg
		]);
	}

	public function create(CreateBookRequest $request)
	{

		$user = auth('api')->user();

		$c_book = Book::create([
			'title' => $request->title,
			'description' => $request->description,
			'isbn' => $request->isbn,
			'admin_id' => $request->admin_id,
			'authors' => json_encode($request->authors)
		]);

		$user->books()->save($c_book);
		
		$book = [
			'data' => [
				'id' => $c_book->id,
				'isbn' => $c_book->isbn,
				'title' => $c_book->title,
				'description' => $c_book->description,
				'admin_id' => $c_book->admin_id,
				'authors' => $this->getAuthorsDetails($c_book->authors),
				'review' => [
					'avg' => 0,
					'count' => 0
				]
			]
		];

		//Add authors and assign relationships
		foreach($this->getAuthorsDetails($c_book->authors) as $author):

			$a_author = Author::create([
				'user_id' => $author['id']
			]);

			$c_book->authors()->save($a_author);
			
		endforeach;

		return response()->json($book);
	}

	private function getAuthorsDetails($authorIds)
	{
		$authorIds = json_decode($authorIds);
		$authors = [];
		foreach($authorIds as $author_id):
			if($this->user_repo->get_author('id', $author_id) != null){
				$author = $this->user_repo->get_author('id', $author_id);
				$authorDetails = [
					'id' => $author_id,
					'name' => $author->name,
					'surname' => $author->surname
				];
				array_push($authors, $authorDetails);
			}
		endforeach;
		return $authors;
	}

	public function handleFilter(FilterRequest $request)
	{
		$page = $request->get('page', 1);
		$sortColumn = $request->get('sortColumn', 'id');
		$sortDirection = $request->get('sortDirection', 'asc');
		$q_title = $request->get('title');
		$q_authors = $request->get('authors');
		$q_paginate = $request->get('paginate', 5);
		$q_title = str_replace(" ", "%", $q_title);
		// $q_authors = explode(',', $q_authors);
		
		$books = Book::query();
		
		if($q_title != null)
		{
			$books = $books
						->where('title', 'like', '%'.$q_title.'%')
						->orWhere('isbn', 'like', '%'.$q_title.'%')
						->orWhere('description', 'like', '%'.$q_title.'%');
		}

		if($q_authors != null){
			$books = $books
						->orWhere('authors', 'like', '%'.$q_authors.'%');
		}

		if($sortColumn != null)
		{
			$books = $books
						->orderBy($sortColumn, $sortDirection);
		}

		$books = $books
					->paginate($q_paginate);

		$bookDetails = $this->setUpResource($books, $page);

		return response()->json($bookDetails);
	}

	private function setUpResource($books, $page)
	{
		$bookDetails = [];
		foreach($books as $book){
			$n_book = [
			'data' => [
				'id' => $book->id,
				'isbn' => $book->isbn,
				'title' => $book->title,
				'description' => $book->description,
				'authors' => $this->getAuthorsDetails($book->authors),
				'review' => [
					'avg' => round(DB::table('reviews')->where('book_id','=',$book->id)->avg('review')),
					'count' => DB::table('reviews')->where('book_id','=',$book->id)->count(),
				]
				],
				'links' => [
					'first' => $books->firstItem(),
					'last' => $books->lastItem(),
					'prev' => $books->previousPageUrl(),
					'next' => $books->nextPageUrl()
				],
				'meta' => [
					'current_page' => $books->currentPage(),
					'from' => $books->firstItem(),
					'last_page' => $books->lastPage(),
					'path' => $books->url($page),
					'per_page' => $books->perPage(),
					'to' => $books->lastItem(),
					'total' => $books->count()
				]
			];
			array_push($bookDetails, $n_book);
		}
		return $bookDetails;
	}
}
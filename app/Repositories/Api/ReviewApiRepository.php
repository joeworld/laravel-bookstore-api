<?php

namespace App\Repositories\Api;

use App\User;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Contracts\RepositoryInterface;
use App\Http\Requests\Api\BookReviewRequest;
use App\Repositories\Api\BookApiRepository as BookRepo;
use Illuminate\Support\Facades\Auth;

class ReviewApiRepository implements RepositoryInterface
{

	private $book_repo;

	public function __construct()
	{
		$this->book_repo = new BookRepo;
	}

    public function get($type, $value)
	{}
    
	public function getAll($order = null, $limit = null)
	{}

	public function create($book_id, BookReviewRequest $request)
	{
		$user = auth('api')->user();
		if($this->book_repo->get('id', $book_id) != null):

			$review = Review::create([
				'review' => $request->review,
				'comment' => $request->comment,
				'book_id' => $book_id
			]);

			$user->reviews()->save($review);

			$review = [
				'data' => [
					'id' => $review->id,
					'review' => $review->review,
					'comment' => $review->comment,
					'user' => [
						'id' => $user->id,
						'name' => $user->name
					]
				]
			];

			$this->book_repo->updateAvgReview($book_id);

			return response()->json($review);
			
		else:
			return response()->json(['errors' => ['message' => 'Page Not Found']], 404);
		endif;
	}
}
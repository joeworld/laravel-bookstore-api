<?php

namespace App\Repositories\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;

class UserApiRepository implements RepositoryInterface
{

	public function get($type, $value)
	{
		return User::where($type, $value)->first()->toJson();
    }

	public function get_author($type, $value)
	{
		return User::where($type, $value)->first();
    }
    
	public function getAll($order = null, $limit = null)
	{
		if($order === null && $limit === null):
			return User::all()
			->toJson();
		elseif($order !== null && $limit === null):
			return User::all()
			->orderBy('id', $order)
			->get()
			->toJson();
		elseif($order === null && $limit !== null):
			return User::all()
			->take($limit)
			->get()->toJson();
		else:
			return User::all()
			->orderBy('id', $order)
			->take($limit)
			->get()
			->toJson();
		endif;
    }
    
	public function login($request)
	{
        $credentials = $request->only(['email','password']);
        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['errors' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    
	public function create(RegisterRequest $request)
	{
		//Create user, generate token and return
        $user =  User::create([
            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'type' => User::DEFAULT_TYPE,
            'password' => Hash::make($request->input('password')),
        ]);
        
        return $this->login($request);
    }

    public function profile()
    {
        return response()->json(auth('api')->user());
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

}
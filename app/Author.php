<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'book_id'
    ];

    /**	
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function author(){
        return $this->belongsTo(Author::class, 'book_id');
    }
}
<?php

namespace App\Factories;

use App\Contracts\FactoryInterface;
use App\Repositories\Api\BookApiRepository;

class BookFactory implements FactoryInterface
{

	static public function create() {
        // return new BookRepository();
    }

	static public function createApi() {
        return new BookApiRepository();
    }

}
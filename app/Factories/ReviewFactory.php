<?php

namespace App\Factories;

use App\Contracts\FactoryInterface;
use App\Repositories\Api\ReviewApiRepository;

class ReviewFactory implements FactoryInterface
{

	static public function create() {
        // return new ReviewRepository();
    }

	static public function createApi() {
        return new ReviewApiRepository();
    }

}
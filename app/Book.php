<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $table = 'books';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isbn', 'title', 'description', 'authors', 'admin_id'
    ];

	/**	
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function admin(){
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function authors(){
        return $this->hasMany(Author::class, 'book_id');
    }

}
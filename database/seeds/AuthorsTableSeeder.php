<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            'user_id' => 1,
            'book_id' => 1
        ]);

        DB::table('authors')->insert([
            'user_id' => 2,
            'book_id' => 1
        ]);

        DB::table('authors')->insert([
            'user_id' => 1,
            'book_id' => 2
        ]);
        DB::table('authors')->insert([
            'user_id' => 2,
            'book_id' => 2
        ]);
    }
}

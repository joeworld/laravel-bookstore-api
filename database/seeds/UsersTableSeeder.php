<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * Admin User
         */

        DB::table('users')->insert([
            'surname' => 'Lincoln',
            'name' => 'Dabbs',
            'type' => 'admin',
            'email' => 'linkcoln@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => now(),
        	'updated_at' =>	now()
        ]);

        /**
         * Basic User
         */

        DB::table('users')->insert([
            'surname' => 'Crowe',
            'name' => 'Dope',
            'email' => 'dope@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => now(),
        	'updated_at' =>	now()
        ]);

    }
}
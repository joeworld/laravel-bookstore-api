<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'title' => 'Write clean code',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem neque, maximus id sollicitudin aliquet, iaculis vitae ligula. Nam ut nisl nec lacus mattis aliquet. Nullam dapibus sodales purus, sit amet tempus ipsum blandit eget. Nullam facilisis tortor ut velit pulvinar molestie nec id sapien.',
            'isbn' => '1234567876454',
            'authors' => json_encode([1,2]),
            'admin_id' => 1,
            'created_at' => now(),
        	'updated_at' =>	now()
        ]);

        DB::table('books')->insert([
            'title' => 'The way life goes',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem neque, maximus id sollicitudin aliquet, iaculis vitae ligula. Nam ut nisl nec lacus mattis aliquet. Nullam dapibus sodales purus, sit amet tempus ipsum blandit eget. Nullam facilisis tortor ut velit pulvinar molestie nec id sapien.',
            'isbn' => '5767067876454',
            'authors' => json_encode([1,2]),
            'admin_id' => 1,
        	'created_at' => now(),
        	'updated_at' =>	now()
        ]);

        DB::table('books')->insert([
            'title' => 'Flower in the Deck',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem neque, maximus id sollicitudin aliquet, iaculis vitae ligula. Nam ut nisl nec lacus mattis aliquet. Nullam dapibus sodales purus, sit amet tempus ipsum blandit eget. Nullam facilisis tortor ut velit pulvinar molestie nec id sapien.',
            'isbn' => '5767543213454',
            'authors' => json_encode([1,2]),
            'admin_id' => 1,
            'created_at' => now(),
        	'updated_at' =>	now()
        ]);
    }
}
<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class FilterBookTest extends TestCase
{
    use DatabaseTransactions;

    public function testBasicBookFilter()
    {
        $response = $this->json('GET', route('books.filter'));
        // dd($response->getContent());
        $response->assertStatus(200);
    }

    public function testBookFilter()
    {
        $response = $this->json('GET', route('books.filter'),['sortColumn' => 'avg_review', 'sortDirection' => 'desc']);
        // dd($response->getContent());
        $response->assertStatus(200);
    }

    /**
     * More Advance Test
     */

}
<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminBookTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A protected method to create a user
     *
     * @return token
     */

    protected function authenticateUser()
    {
        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@gmail.com',
            'password' => Hash::make('secret123$')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    protected function authenticateAdmin()
    {
        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@gmail.com',
            'type'  => 'admin',
            'password' => Hash::make('secret123$')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    /**
     * Admin can create books
     */

    public function testAdminCanCreateBook()
    {

        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@testauthor.com',
            'password' => Hash::make('secret123$')
        ]);
        $author = $user;

        $user2 = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@testauthor2.com',
            'password' => Hash::make('secret123$')
        ]);
        $author2 = $user2;

        //User's Data
        $data = [
            'title' => 'This a Book for All',
            'description'  => 'This is a book for you and all of you',
            'isbn' => '1239037621873',
            'authors' => [$author->id, $author2->id]
        ];

        $token = $this->authenticateAdmin();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('books.store'), $data);

        // dd($response->getContent());
        $response->assertStatus(200);
    }

    /**
     * Non Admin cannot create books
     */

    public function testNonAdminCannotCreateBook()
    {

        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@testauthor.com',
            'password' => Hash::make('secret123$')
        ]);
        
        $author = $user;

        $user2 = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@testauthor2.com',
            'password' => Hash::make('secret123$')
        ]);
        $author2 = $user2;

        //User's Data
        $data = [
            'title' => 'This a Book for All',
            'description'  => 'This is a book for you and all of you',
            'isbn' => '1239037621873',
            'authors' => [$author->id, $author2->id]
        ];

        $token = $this->authenticateUser();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('books.store'), $data);

        // dd($response->getContent());
        $response->assertStatus(401);
    }

}
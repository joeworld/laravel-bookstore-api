<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A protected method to create a user
     *
     * @return token //
     */

    protected function authenticate()
    {
        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@gmail.com',
            'password' => Hash::make('secret123$')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    /**
     * Test User Registration
     * @test
     */

    public function testRegister()
    {
        //User's Data
        $data = [
            'email' => 'test@gmail.com',
            'name'  => 'Test User',
            'surname' => 'D Andre',
            'password' => 'secret123$',
            'password_confirmation' => 'secret123$'
        ];

        //Send the post request
        $response = $this->json('POST', route('api.register'), $data);
        //Assert that it was successful
        $response->assertStatus(200);
        //Assert that a token was received
        $this->assertArrayHasKey('access_token', $response->json());
    }

    /**
     * Test User Login
     * @test
     */
    public function testLogin()
    {
        User::create([
            'email' => 'test@gmail.com',
            'name'  => 'Test User',
            'surname' => 'D Andre',
            'password' => Hash::make('secret123$'),
            'password_confirmation' => 'secret123$'
        ]);

        $data = [
            'email' => 'test@gmail.com',
            'password' => 'secret123$',
        ];

        //attempt login
        $response = $this->json('POST', route('api.login'), $data);
        //Assert it was successful and a token was received
        // dd($response->getContent());
        $response->assertStatus(200);
        $this->assertArrayHasKey('access_token', $response->json());

    }

    /**
     * Test User Profile
     * @test
     */
    public function testProfile()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('api.me'), []);

        $response->assertStatus(200);
    }

    /**
     * Test User Logout
     * @test
     */
    public function testLogout()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('api.logout'), []);

        $response->assertStatus(200);
    }

}
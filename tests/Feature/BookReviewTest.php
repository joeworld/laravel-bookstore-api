<?php

namespace Tests\Feature;

use App\User;
use App\Review;
use App\Book;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class BookReviewTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A protected method to create a user
     *
     * @return token
     */

    protected function authenticateUser()
    {
        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@gmail.com',
            'password' => Hash::make('secret123$')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    protected function authenticateAdmin()
    {
        $user = User::create([
            'name' => 'Test User',
            'surname' => 'Dr. Test',
            'email' => 'test@gmail.com',
            'type'  => 'admin',
            'password' => Hash::make('secret123$')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    /**
     * Admin can create books
     */

    public function createBook()
    {
		$book = Book::create([
			'title' => 'Learn to be just cool',
			'description' => 'Single Handsome Boring But Awesome',
			'isbn' => 1234567890123,
			'admin_id' => 4,
			'authors' => json_encode([4])
        ]);

        return $book;
        
    }

    public function testUserCanMakeReview()
    {
        $book = $this->createBook();
        $data = [
            'review' => '10',
            'comment' => 'I enjoyed this book so much, what a perfect ending'
        ];

        $token = $this->authenticateUser();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('book.store.review', ['id' => $book->id]), $data);
        
        $response->assertStatus(200);
    }

    public function testGuestUserCannotReview()
    {

        $book = $this->createBook();
        $data = [
            'review' => '10',
            'comment' => 'I enjoyed this book so much, what a perfect ending'
        ];

        $response = $this->json('POST', route('book.store.review', ['id' => $book->id]), $data);
        
        $response->assertStatus(401);
    }

    public function testAdminCanMakeReview()
    {
        $book = $this->createBook();
        $data = [
            'review' => '10',
            'comment' => 'I enjoyed this book so much, what a perfect ending'
        ];

        $token = $this->authenticateAdmin();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('book.store.review', ['id' => $book->id]), $data);
        
        // dd($response->getContent());
        $response->assertStatus(200);
    }

}
# Laravel 6.4.1 Bookstore Application Using the TDD and Solid Approach.

The frontend app can be found at https://gitlab.com/joeworld/react-redux-app-for-laravel-bookstore

# Installation

You should know how to setup jwt authentication as this project uses tymon json web token to validate client https://github.com/tymondesigns/jwt-auth, and its uses barryvdh/laravel-cors https://github.com/barryvdh/laravel-cors for cors

1. Copy .env.example and change to .env
2. run ```composer update``` to install all dependencies
3. Generate application key ```php artisan key:generate```
4. Generate jwt key ```php artisan jwt:secret```
5. Run migrations with ```php artisan migrate```
6. Run seedings with ```php artisan db:seed```
5. Run ```php artisan serve``` to kickstart
6. Check all registered endpoints using ```php artisan route:list```

# Tests

This application comes with feature tests and to ensure your're up and running ```vendor/bin/phpunit ./tests``` to run all available tests

# Design Patterns Used

1. Repository Design Patterns
2. Factory Design Patterns